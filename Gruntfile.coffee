#bower = require "bower"

appFolder = "dist"
tempFolder = ".tmp"
angularAppName = "jl246"
templatePathPrepend = ""
# true/false or port
livereload = true

module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON "package.json"
    appName: "jl246"


    source:
      browser: "app/browser"

    temp:
      main: ".tmp"
      browser: "<%= temp.main %>/browser"




#    copy:
#      js:
#        files: [
#          {expand: true, cwd:"dev/components", src: ["**.js"], dest: "dist/js/"}
#        ]
#      css:
#        files: [
#          { expand: true, cwd:"dev/components/",src: ["**.css"], dest: "dist/app/css/"}
#        ]
#      gif:
#        files: [
#          { expand: true, cwd:"dev/components/",src: ["edit.gif","loading.gif","processing.gif"], dest: "#{appFolder}/app/css/"}
#        ]


    clean:
      browserTemplate: [ "<%= jade.browser.dest %>" ]
#      templates: [ "#{appFolder}/app/**.html", "!#{appFolder}/app/index.html" ]
#      css: [ "public/app/css" ]
#      js: [ "public/app/js" ]



    jade:
      init:
        options: pretty: true
        expand: true
        cwd: "<%= source.browser %>"
        src: ["head.jade", "foot.jade"]
        dest: "<%= temp.main %>/jade"
        ext: ".html"

      browser:
        expand: true
        cwd: "<%= source.browser %>"
        src: ["**/*.jade", "!index.jade", "!head.jade", "!foot.jade", "!**/*-nojs.jade"]
        dest: "<%= temp.browser %>/html"
        ext: ".html"

    concat:
      coffeebrowser:
        src: ["<%= source.browser %>/social.coffee","<%= source.browser %>/app.coffee","<%= source.browser %>/**/*.coffee"]
        dest: "<%= temp.browser %>/js/app.coffee"

      lessbrowser:
        src: ["<%= source.browser %>/styles.less","<%= source.browser %>/**/*.less"]
        dest: "<%= temp.browser %>/css/styles.less"

    coffee:
      options:
        sourceMap: true
      browser:
        files:
          "<%= temp.browser %>/js/app.js": ["<%= concat.coffeebrowser.dest %>"]

    ngmin:
      browser:
        expand: true
        cwd: "<%= temp.browser %>/js"
        src: ["**/*.js"]
        dest: "<%= temp.browser %>/js"
        ext: ".js"

    less:
      browser:
        files: "<%= temp.browser %>/css/styles.css": ["<%= concat.lessbrowser.dest %>"]

    autoprefixer:
      browser:
        src: "<%= temp.browser %>/css/styles.css"
        dest: "<%= temp.browser %>/css/styles.css"


    ngtemplates:
      browser:
        options:
          url: (url) -> "/static/browser/html/" + url
          module: "<%= appName %>"
        cwd: "<%= jade.browser.dest %>"
        src: ["**/*.html"]
        dest: "<%= temp.browser %>/js/templates.js"


    useminPrepare:
      browser:'<%= jade.init.dest %>/**/*.html'
      options:
        dest: 'public'

    usemin:
      html: '<%= jade.init.dest %>/**/*.html'


    htmlbuild:
      dist:
        src: '<%= jade.init.dest %>/head.html'
        dest: '<%= jade.init.dest %>'


    watch:
      options:
        atBegin: true
        livereload: livereload
        interrupt: true
      other:
        files: ['<%= source.browser %>/index.jade']
        tasks: []

      browsercoffee:
        files: ['<%= concat.coffeebrowser.src %>']
        tasks: ["concat:coffeebrowser", "coffee:browser"]

      browserless:
        files: ['<%= concat.lessbrowser.src %>']
        tasks: ["concat:lessbrowser", "less:browser", "autoprefixer:browser"]

      browserinit:
        files: ['<%= source.browser %>/head.jade', '<%= source.browser %>/foot.jade']
        tasks: ["jade:init"]

      browsertemplates:
        files: ['<%= source.browser %>/**/*.jade', '!<%= source.browser %>/*.jade', "!<%= source.browser %>/**/*-nojs.jade"]
        tasks: ["clean:browserTemplate", "jade:browser", "ngtemplates:browser"]


  grunt.registerTask "browser",[
    "concat:coffeebrowser"
    "coffee:browser"

    "concat:lessbrowser"
    "less:browser"
    "autoprefixer:browser"

    "clean:browserTemplate"
    "jade:init"

    "jade:browser"
    "ngtemplates:browser"
  ]

  grunt.registerTask "browserProd",[
    "browser"
    "ngmin:browser"
    "htmlbuild"

    "useminPrepare:browser"
    "concat:generated"
    "uglify:generated"
    "cssmin:generated"
    "usemin"
  ]


  grunt.loadNpmTasks "grunt-contrib-concat"
  grunt.loadNpmTasks "grunt-contrib-less"
  grunt.loadNpmTasks "grunt-contrib-cssmin"
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-copy"
  grunt.loadNpmTasks "grunt-contrib-clean"
  grunt.loadNpmTasks "grunt-contrib-jade"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-usemin"
  grunt.loadNpmTasks "grunt-angular-templates"
  grunt.loadNpmTasks "grunt-html-build"
  grunt.loadNpmTasks "grunt-ngmin"
  grunt.loadNpmTasks 'grunt-autoprefixer'


