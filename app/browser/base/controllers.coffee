

#CLIENT ID: "64f0af7e1d3944b38f171d6f634a1771"
#CLIENT SECRET: "eb16a183a4ea4edf844d7fa48dd45f0b"
#access_token 21705797.64f0af7.70a38d064101424e9f8d6a65507c93bf
#  url = "https://api.instagram.com/v1/users/21705797/media/recent?access_token=21705797.64f0af7.70a38d064101424e9f8d6a65507c93bf&callback=JSON_CALLBACK"
#  $http.jsonp(url).success (data) =>
#    @data =  data
#

app.controller 'homeCtrl', (item, user) ->




#  $http.jsonp( "http://urls.api.twitter.com/1/urls/count.json?callback=JSON_CALLBACK&url=http://justlooking246.com")
#  .success (data, status) =>
#    @tweets = data.count

  return


app.controller 'searchCtrl', ($scope, $http, item, category, $state) ->
  @pagination = {}
  @pagination.page = if $state.params?.page > 0 then $state.params.page - 1  else 0
  @pagination.step = 12
  @pagination.start = => @pagination.page * @pagination.step
  @pagination.limit = => @pagination.start() + @pagination.step
  @pagination.pageCount = => Math.ceil(@productList?.length / @pagination.step) or 1
  @pagination.go = (page) =>
    if page >= 0
#      @pagination.page = page
#      filter()
      $state.go '.', {page: +page+1}

  @goCategory = (catID) =>
    if +catID is +$state.params.category
      $state.go '.', {category: null, page: null}
    else
      $state.go '.', {category: +catID, page: null}


  @categories = => category.list
  item.get().then (a) =>
    filter()
#  category.get().then (a) =>
#    @categories = a

  filter = =>
    @productList = item.list
    @productList = (x for x in @productList when +x.category_id is +$state.params.category) if +$state.params?.category


    @products = @productList[ @pagination.start()  ... @pagination.limit()]

  return



app.controller 'itemCtrl', ($stateParams, item, user) ->
  @params = $stateParams

  item.find($stateParams.id).then (a) =>
    @data =  a

  @canEdit = =>
#    return true if user.data?.is_superuser
#    console.log @data
#    _.filter( user.data?.store_permissions,{company_id: store.id, view: true}).length > 0

  return


app.controller 'storesCtrl', (storeService) ->

  storeService.get().then (a) =>
    @data =  a

  return


app.controller 'storeCtrl', (store, storeService, user) ->
  @data = store

  @canView = =>
    return true if user.data?.is_superuser
    _.filter( user.data?.store_permissions,{company_id: store.id, view: true}).length > 0

  @canEdit = =>
    return true if user.data?.is_superuser
    _.filter( user.data?.store_permissions,{company_id: store.id, view: true, edit:true}).length > 0

  return


