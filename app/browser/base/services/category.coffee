
app.service 'category', ($rootScope ,util, $q, primus) ->
  @list = []
  fetched = false

  @get = (data) =>
    defer = $q.defer()

    if fetched
      defer.resolve(@list)
    else
      
      primus.send 'get:category', data, (err, data) =>
        console.log err if err
        util.addToList @list, data, 'id'
        fetched = true
        defer.resolve(@list)

    return defer.promise

  @post = (data, callback) =>
    primus.send 'post:category', data, (err, data) =>

      d = angular.copy( data )
      util.addToList @list, d

      callback?(null, data)




  return

