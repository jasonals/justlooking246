
app.service 'item', ($rootScope ,util, $q, primus, category) ->
  @list = []
  fetched = false

  @get = (data) =>
    defer = $q.defer()

    if fetched
      defer.resolve(@list)
    else
      primus.send 'get:item', data, (err, data) =>
        console.log err if err
        util.addToList @list, data, 'id'
        fetched = true
        defer.resolve(@list)

        category.get({id: _(@list).pluck('category_id').unique().compact().value()})

    return defer.promise

  @post = (data, callback) =>
    primus.send 'post:item', data, (err, data) =>

      d = angular.copy( data )
      d.description = undefined
      util.addToList @list, d

      callback?(null, data)

  @updateList = (data) =>
    d = angular.copy( data )
    d.description = undefined if d.description
    util.addToList @list, d


  @find = (id, field="id") =>
#    console.log id, field
    defer = $q.defer()

    result =  _.find @list, (object) ->
      ''+object[field] is ''+id  if object[field]

#    if result
#      defer.resolve result

    query = {}
    query[field] = id
    primus.send 'get:item', query, (err, data) =>
      console.log err if err

      d = angular.copy( data[0] )
      d.description = undefined
      util.addToList @list, d, field

      defer.resolve data[0]
    return defer.promise


  primus.on "get:item", (data) ->
    util.addToList @list, data, 'id'


  return

