
app.service 'storeService', ($rootScope ,util, $q, primus) ->
  @list = []
  fetched = false

  @get = =>
    defer = $q.defer()

    if fetched
      defer.resolve(@list)

    primus.send 'get:store',null, (err, data) =>
      console.log err if err
      util.addToList @list, data, 'id'

      fetched = true
      defer.resolve(@list)

    return defer.promise

  @post = (data, callback) =>
    primus.send 'post:store', data, (err, data) =>
      console.log err if err
      util.addToList( @list, data, 'id') if data?
      callback?(data)

  @find = (id, field="id") =>
    defer = $q.defer()

#    field = field or "id"
    result =  _.find @list, (object) ->
      ''+object[field] is ''+id  if object[field]

    if result
      defer.resolve result
    else
      query = {}
      query[field] = id
      primus.send 'get:store', query, (err, data) =>
        console.log err if err
        util.addToList @list, data, 'id'
        defer.resolve data[0]
    return defer.promise


  primus.on "get:store", (data) ->
    util.addToList @list, data, 'id'


  return

