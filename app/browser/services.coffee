



app.directive 'pagination', ->
  restrict: 'AE'
  templateUrl: "#{baseTemplateUrl}/base/pagination.html"
  scope:
    pagination: '=bind'
  link: (scope, elem, attrs) ->

    scope.pagination.pageNext = () =>
      scope.pagination.go?(+scope.pagination.page + 1)
    scope.pagination.pagePrevious = () =>
      scope.pagination.go?(+scope.pagination.page - 1)



app.service 'util', ->

  addToList: (list, data, field) ->
    field = field or 'id'
    addObject = (object) ->
      found = _.find list, (obj) ->
        obj[field] is object[field] if object[field]
      if found
        _.merge(found, object)
      else
        list.push object

    if _.isArray(data)
      _.each data, (object) -> addObject object
    else
      addObject data if _.isObject data


  addOrRemoveFromList: (list, data, field) ->
    field = field or 'id'
    handleObject = (object) ->
      found = _.indexOf list, _.find list, (obj) ->
        obj[field] is object[field] if object[field]

      if found > -1
        list.splice found, 1
      else
        list.push object

    if _.isArray(data)
      _.each data, (object) -> handleObject object
    else
      handleObject data if _.isObject data


app.directive 'fileInput',  ->
  inp = "<input type='file' accept='image/*' name='image' style='display: none;'>"
  image = '<img class="img-responsive">'

  template: """
        <button class='btn' type="button" ng-click='clickMe()'>Choose File</button>
        <button class='btn' type="button" ng-click='clickMe(true)' ng-if='main.device.isMobile'>Take Picture</button>
        <span class='h4'>{{ fileName }}</span>
  """

  link: (scope, elem, attrs) ->
    inpu = angular.element(inp)
    elem[0].appendChild(inpu[0])

    img = angular.element(image)
    elem[0].appendChild(img[0])

    scope.clickMe = (capture) ->
      if capture
        inpu[0].setAttribute('capture','camera')
      else
        inpu[0].removeAttribute('capture')
      inpu.click()

    inpu[0].addEventListener 'change', (evt) ->
      img[0].src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw=='

      unless FileReader?
        scope.fileName = inpu.val().split?('\\').pop?()
        scope.$apply()
      else
        for f in evt.target?.files
          do (f) ->
            reader = new FileReader()
            reader.onload = (theFile) ->
              img[0].src = theFile.target.result
            reader.readAsDataURL(f)




