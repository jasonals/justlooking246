
app.service 'primus', ($rootScope) ->

  @primus = new Primus ""

  @send = (name, data, callback) =>
    @primus.send name, data, =>
      args = arguments
      callback?.apply @primus, args
      $rootScope.$apply()

  @on = (name, callback) =>
    @primus.on name, =>
      args = arguments
      callback?.apply @primus, args
      $rootScope.$apply()


  return
