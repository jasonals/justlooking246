

app.service 'stock', ($rootScope ,util, $q, primus, $timeout) ->
  @list = []

  @get = (data) =>
    defer = $q.defer()

    if tmp = _.filter(@list, {company_id:data})
      $timeout =>
        defer.notify tmp
      ,0

    primus.send 'get:stock', data, (err, result) =>
      console.log err if err
      util.addToList @list, result, 'id'
      defer.resolve _.filter(@list, {company_id:data})
    return defer.promise

  @find = (id, field="id") =>
    defer = $q.defer()

    result =  _.find @list, (object) ->
      ''+object[field] is ''+id  if object[field]

    if result
      defer.resolve result

    query = {}
    query[field] = id
    primus.send 'get:stock', query, (err, data) =>
      console.log err if err
      util.addToList @list, data, "id"
      defer.resolve data[0]
    return defer.promise

  @post = (data, callback) =>
    primus.send 'post:stock', data, (err, result) =>
      console.log err, result
      util.addToList( @list, data, 'id') if data?
      callback?(null, true)



  primus.on "get:stock", (data) ->
    util.addToList @list, data, 'id'

  return



app.service 'storePermission', ($rootScope ,util, $q, primus) ->
  @list = []

  @get = (data) =>
    defer = $q.defer()

    if (s for s in @list when s.company_id is data ).length
#      defer.resolve(@list)
      defer.resolve (s for s in @list when s.company_id is data )

    primus.send 'get:storePermission', {company_id: data}, (err, result) =>
      console.log err if err
      util.addToList @list, result, 'id'

      defer.resolve (s for s in @list when s.company_id is data )

    return defer.promise




  primus.on "get:storePermission", (data) ->
    util.addToList @list, data, 'id'


  return
