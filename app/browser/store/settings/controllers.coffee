app.controller 'storeSettingsInfoCtrl', (store, storeService) ->
  @data =  angular.copy store

  @update = =>
    return unless @temp
    storeService.post @temp, (data) =>
      console.log data, 'controller'

  @reset = =>
    @data =  angular.copy store


  @difference = =>
    temp = undefined
    for key of @data
      if @data[key] isnt store[key]
        temp ?= {id: store.id}
        temp[key] = @data[key]

    @temp = temp



  return



app.controller 'storeSettingsPermissionCtrl', (store, storePermission, $state) ->
  storePermission.get(store.id).then (b) =>
    @loaded = true
    @permissions = b


#  filter = =>
#    @permissions = (s for s in storePermission.list when s.company_id is store.id )

#    @stock = angular.copy @stockList[ @pagination.start()  ... @pagination.limit()]


  return

app.controller 'storeSettingsStockCtrl', ($scope, stock, store, $state) ->
  @pagination = {}
  @pagination.page = if $state.params?.page > 0 then $state.params.page - 1  else 0
  @pagination.step = 12
  @pagination.start = => @pagination.page * @pagination.step
  @pagination.limit = => @pagination.start() + @pagination.step
  @pagination.pageCount = => Math.ceil(@stockList?.length / @pagination.step)
  @pagination.go = (page) =>
    if page >= 0
      @pagination.page = page
      filter()
      $state.go '.', {page: +page+1}

  @difference = =>
    return unless stock.list?.length
    temp = undefined
    if @stock?
      for s in @stock
        st = _.find(stock.list, {id: s.id})
        tt = undefined
        for key of s
          continue if '$' is key[0]
          continue if not s[key] and not st[key]
          if s[key] isnt st[key]
            tt ?= {id: st.id}
            tt[key] = s[key]
        temp ?= [] if tt
        temp.push(tt) if tt

    @tempId = _.pluck temp, 'id'
    @temp = temp

  @edited = (id) ->
    id in @tempId

  @reset = ->
    filter?()

  @update = =>
    return unless @temp
    stock.post @temp, (err, data) =>

  show = (b) =>
    @stockList = _.sortBy( b, (o) -> -o.id)
    @loaded = true
    filter()

  filter = =>
#      @stockList = _.sortBy (s for s in stock.list when s.company_id is store.id ), (o)-> -o.id

    @stock = angular.copy @stockList[ @pagination.start()  ... @pagination.limit()]

  if $scope.store?.canView?()

    stock.get(store.id).then (b) =>
      show(b)
    ,( () -> return )
    ,( (b) => show(b) )



  return



app.controller 'editItemCtrl', ($scope, item) ->
  original = null
  @getItem = (id) =>
    item.find(id).then (a) =>
      @item =  angular.copy a
      original = a

  @reset = =>
    @item = angular.copy original

  @difference = =>
    return unless original and @item?
    temp = undefined
    for key of @item
#      continue if '$' is key[0]
      continue if not @item[key] and not original[key]
      continue if key is 'store'
      if @item[key] isnt original[key]
        temp ?= {id: original.id}
        temp[key] = @item[key]

    @temp = temp

  @save = =>
    item.post @difference(), (err, data) =>
      console.log data, "asdadad"
      @item = angular.copy data
      original = data

  @uploadComplete = (content, completed) =>
    if completed and content.item
      item.updateList(content.item)
      @item =  angular.copy content.item
      original = content.item
      $scope.$apply()

      return

  return


app.directive 'editItem', ($compile, $templateCache) ->

  restrict: 'E'
#  controller: "editItemCtrl as item"
  link: (scope, elem, attrs) ->
#    elem.modal( show: false  )
    elem.addClass('modal')

    scope.modal ?= {}
    scope.modal.hide = ->
      elem.modal('hide')
      return

    elem.on 'hidden.bs.modal', ->
#      newScope.$destroy()
      elem.html("")
      return


    scope.modal.editItem = (id)->
      elem.html($templateCache.get("#{baseTemplateUrl}/store/settings/item-edit.html"))
      $compile(elem.contents())(scope)
      angular.element(elem.children()[0]).scope().item.getItem(id)
      elem.modal()
      return


