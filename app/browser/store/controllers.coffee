
app.controller 'storeSearchCtrl', ($scope, $http, item, $state, store, category) ->
  @pagination = {}
  @pagination.page = if $state.params?.page > 0 then $state.params.page - 1  else 0
  @pagination.step = 12
  @pagination.start = => @pagination.page * @pagination.step
  @pagination.limit = => @pagination.start() + @pagination.step
  @pagination.pageCount = => Math.ceil(@productList?.length / @pagination.step)

  @pagination.go = (page) =>
    if page >= 0
#      @pagination.page = page
#      filter()
      $state.go '.', {page: +page+1}

  @goCategory = (catID) =>
    if +catID is +$state.params.category
      $state.go '.', {category: null, page: null}
    else
      $state.go '.', {category: +catID, page: null}


  item.get().then (b) =>
    filter()

  @categories = =>
    cats = _(@allProducts).pluck('category_id').unique().compact().value()
    _.filter category.list, (o) => o.id in cats

  filter = =>
    @allProducts = @productList = (i for i in item.list when (b for b in i.store when b.company_id is store?.id).length )
    @productList = (x for x in @productList when +x.category_id is +$state.params.category) if +$state.params?.category

    @products = @productList[ @pagination.start() ... @pagination.limit()]

  return
