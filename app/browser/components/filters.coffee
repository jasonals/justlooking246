app.filter 'price', ->
  return (data) ->
    val = _(data).pluck("price_retail").filter((val)-> val>0 ).max().value()

    return val or 0
