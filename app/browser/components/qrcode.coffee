

app.directive "qrCode", () ->
  (scope, element, attrs) ->
    qr = null
    url = null
    scope.$watch (-> attrs.qrCode ), ->
      if attrs.qrCode
        if not qr
          qr = new QRCode element[0],
            text: attrs.qrCode
            width: attrs.size or 150
            height: attrs.size or 150
            colorDark : attrs.color or "#000000"
            colorLight : "#ffffff"
            correctLevel : QRCode.CorrectLevel.H
        else if attrs.qrCode isnt url
          qr.makeCode attrs.qrCode
        url = attrs.qrCode
#        qr.addData attrs.qrCode
#        qr.make()
#        element[0].innerHTML = qr.createImgTag(4)


