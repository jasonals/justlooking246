app.directive 'a', ($location) ->

  restrict: 'E'
  link: (scope, elem, attrs) ->

    return if _.intersection(_ .keys(attrs), ['ngClick']).length
    parent = elem.parent()


    scope.$watch (->$location.path() + attrs.href), ->
      if attrs.href is $location.path() or attrs.href is "#"+$location.path()
        parent.addClass('active') if parent.children().length is 1
        elem.addClass('active')
      else
        parent.removeClass('active') if parent.children().length is 1
        elem.removeClass('active')


