
app.controller 'loginCtrl', (user) ->
  @email = null
  @password = null

  @login = =>
    return unless @email? and @password?
    user.login @email, @password, (err, result) =>
      if result
        @email = null
        @password = null

  return


app.controller 'userCtrl', ($stateParams, profile) ->
  @data = profile

  return



app.controller "userActionCtrl", ( itemAction, profile) ->
  itemAction.find(profile.id, "user_id").then (a) =>
    @data = a

  return

