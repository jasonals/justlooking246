
app.service 'user', (primus) ->

  @data = {}

  primus.on 'open', =>
    if token = store.get("token")
      primus.send "post:tokenLogin", {token: token}, (err, user) =>
        _.extend @data, user



  @login = (username, password, callback) =>
    store.remove("token")
    @data[field] = undefined for field of @data

    primus.send "post:login", {email: username, password: password}, (err, user) =>
      store.set('token', user.token) if user?.token?
      _.extend(@data, user) if user?
      callback? err, user

  @fblogin = =>
    FB.login (response) =>
#      console.log response
      primus.send 'post:fblogin', { userID: response.authResponse.userID, accessToken: response.authResponse.accessToken }, (err, user) =>
#        console.log err, user
        store.set('token', user.token) if user?.token?

        _.extend(@data, user) if user?


    , {scope: 'email, publish_actions'}

  @googlelogin = (authResult) =>
    console.log authResult

  @logout = (callback) =>
    store.remove("token")
    for field of @data
      @data[field] = undefined
      delete @data[field]

    callback?()


  return


app.service 'profile', ($rootScope ,util, $q, primus) ->
  @list = []


  @find = (id, field="id") =>
    defer = $q.defer()

    result =  _.find @list, (object) ->
      ''+object[field] is ''+id  if object[field]

    if result
      defer.resolve result

    query = {}
    query[field] = id
    primus.send 'get:userProfile', query, (err, data) =>
      console.log err if err
      console.log data
      if data?
        d = angular.copy( data )
        util.addToList @list, d, 'id'
        defer.resolve data
      else
        console.log "rejected"
        defer.reject()

    return defer.promise

  primus.on "get:userProfile", (data) ->
    util.addToList @list, data, 'id'

  return


app.service 'itemAction', ($rootScope ,util, $q, primus) ->

  @list = []


  @find = (id, field="id") =>
    defer = $q.defer()

    result = =>
      _.filter @list, (object) ->
        ''+object[field] is ''+id  if object[field]

    if result().length
      defer.resolve result()

    query = {}
    query[field] = id
    primus.send 'get:itemAction', query, (err, data) =>
      console.log err if err
#      console.log data
      if data?
        util.addToList @list, data, 'id'
        defer.resolve data
      else
        defer.reject()

    return defer.promise

  primus.on "get:itemAction", (data) ->
    util.addToList @list, data, 'id'

  return


app.directive "gSignin", (user) ->
  restrict: 'AC'
  link: (scope, element, attributes) ->
    unwatch = scope.$watch (-> not _.isUndefined gapi?.signin), (a)->
      if a
        unwatch()

        gapi.signin.render element[0],
            clientid: "1072490598166.apps.googleusercontent.com"
            #          requestvisibleactions: 'http://schemas.google.com/AddActivity http://schemas.google.com/CommentActivity'
            cookiepolicy: "http://justlooking246.com"
            scope: "https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me"
#            theme: "dark"
            apppackagename: "com.justlooking246.app"
            redirecturi: "postmessage"
            callback: user.googlelogin

        loginFinishedCallback = (authResult) ->
          if authResult
            if authResult['error'] is undefined
              gapi.auth.setToken(authResult)

              gapi.client.load 'oauth2', 'v2', ->
                gapi.client.oauth2.userinfo.get().execute (obj) ->
                  console.log "info", obj

              gapi.client.load 'plus', 'v1', ->
                gapi.client.plus.people.get({userId: 'me'}).execute (obj) ->
                  console.log "userinfo", obj

            else
              console.log('An error occurred')

          else
            console.log('Empty authResult')



