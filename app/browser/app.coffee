
baseTemplateUrl = "/static/browser/html"


app = angular.module "jl246", [
  "ui.router"
  "ngAnimate"
  "ngTouch"
  "ngUpload"
]


app.config ($stateProvider, $urlRouterProvider, $locationProvider) ->
#  $urlRouterProvider.otherwise '/'
  $locationProvider.html5Mode(true)



  $stateProvider.state 'base',
    templateUrl: "#{baseTemplateUrl}/base/index.html"
#    url: '/'

  $stateProvider.state 'base.home',
    templateUrl: "#{baseTemplateUrl}/base/home.html"
    url: '/'
    controller: 'homeCtrl as home'

  $stateProvider.state 'base.search',
    templateUrl: "#{baseTemplateUrl}/base/search.html"
    url: '/search?page&category'
    controller: 'searchCtrl as search'

  $stateProvider.state 'base.product',
    templateUrl: "#{baseTemplateUrl}/base/product.html"
    url: '/product/{id}'
    controller: 'itemCtrl as item'

  $stateProvider.state 'base.stores',
    templateUrl: "#{baseTemplateUrl}/base/stores.html"
    url: '/stores'
    controller: 'storesCtrl as stores'



#app.config ($stateProvider) ->

  $stateProvider.state 'base.admin',
    templateUrl: "#{baseTemplateUrl}/admin/index.html"
    url: '/admin'
    controller: 'adminCtrl as admin'



#app.config ($stateProvider) ->

  $stateProvider.state 'base.store',
    templateUrl: "#{baseTemplateUrl}/store/store.html"
    url: '/store/{slug}'
    abstract: true
    controller: 'storeCtrl as store'
    resolve:
      store: ['storeService', '$stateParams', (storeService, $stateParams) ->
        storeService.find($stateParams.slug, "slug")
      ]


  $stateProvider.state 'base.store.products',
    templateUrl: "#{baseTemplateUrl}/base/search.html"
    url: "?page&category"
    controller: 'storeSearchCtrl as search'

  $stateProvider.state 'base.store.info',
    templateUrl: "#{baseTemplateUrl}/store/info.html"
    url: "/info"
#    controller: 'searchCtrl as search'



  $stateProvider.state 'base.storeSettings',
    templateUrl: "#{baseTemplateUrl}/store/settings/settings.html"
    url: "/store/{slug}/settings"
    abstract: true
    controller: 'storeCtrl as store'
    resolve:
      store: ['storeService', '$stateParams',(storeService, $stateParams) ->
        storeService.find($stateParams.slug, "slug")
      ]

  $stateProvider.state 'base.storeSettings.info',
    templateUrl: "#{baseTemplateUrl}/store/settings/info.html"
    url: ""
    controller: 'storeSettingsInfoCtrl as storeInfo'



  $stateProvider.state 'base.storeSettings.stock',
    templateUrl: "#{baseTemplateUrl}/store/settings/stock.html"
    url: "/stock"
    controller: 'storeSettingsStockCtrl as stock'

  $stateProvider.state 'base.storeSettings.permissions',
    templateUrl: "#{baseTemplateUrl}/store/settings/permissions.html"
    url: "/permissions"
    controller: 'storeSettingsPermissionCtrl as permissions'



#app.config ($stateProvider) ->

  $stateProvider.state 'base.user',
    url: '/user/:id'
    templateUrl: "#{baseTemplateUrl}/user/index.html"
    controller: 'userCtrl as user'
    resolve:
      profile: ['profile', '$stateParams', (profile, $stateParams) ->
        profile.find($stateParams.id, "id")
      ]

  $stateProvider.state 'base.user.likes',
    url: '/likes'
    templateUrl: "#{baseTemplateUrl}/user/likes.html"
    controller: "userActionCtrl as action"

  $stateProvider.state 'base.user.settings',
    url: '/settings'
    templateUrl: "#{baseTemplateUrl}/user/settings.html"
    resolve:
      allow: ['profile', 'user', (profile, user) ->
        profile.id is user.data?.id
      ]
  $stateProvider.state 'base.login',
    templateUrl: "#{baseTemplateUrl}/user/login.html"
    url: '/login'



app.run ($rootScope, $state, $location) ->
  $rootScope.$state = $state
  $rootScope.$location = $location

  $rootScope.$on "$stateChangeError", (event, toState, toParams, fromState, fromParams, error)->
    console.log "aaaaaaaaaaa", event, toState, toParams, fromState, fromParams, error, "aaaaaaaaaaaaaa"


#primus = null

app.controller 'baseCtrl', ($scope, $window, user, $rootScope) ->

  @user = user



  (calc = _.debounce =>
    $scope.$apply =>
      @window =
        width: $window.innerWidth or $window.innerWidth
        height: $window.innerHeight or $window.innerHeight
  , 1000)

  angular.element($window).on 'resize', (e) =>
    calc()

  return
