

async = require 'async'
_ = require 'lodash'


module.exports = (table) ->

  getStore: (data,callback) ->
    get(table.store, data, callback)

  postStore: (data,callback) ->

    table.store.get data.id, (err, result) ->
      return callback? err, result if err

      for field of result
        result[field] = data[field] if data[field]?

      result.save (err) ->
        callback? err, result





  getStorePermission: (data,callback) ->
    async.waterfall [
      (callback) ->
        get(table.storePermissions, data, callback)
      (perms, callback) ->
        id = _.pluck(perms,"user_id")
        tempPerms = _.cloneDeep(perms)

        table.user.find({id: id}).only('id','first_name','last_name','email').run (err, users) ->

          for user in users
            perm = _.find( tempPerms, {user_id: user.id})
            perm.name = "#{user.first_name} #{user.last_name}"
            perm.email = user.email
          callback err, tempPerms

    ], callback


  getStock: (data,callback) ->

    async.waterfall [
      (callback) ->
        table.storeStock.find({company_id: data}).run callback
      (stocks, callback) ->
        id = _.chain(stocks).pluck('product_id').uniq().value()

        table.item.find({id: id}).run (err, items) ->
          for stock in stocks
            item = _.find( items, {id: stock.product_id})
            stock.name = item.name
            stock.model_name = item.model_name

          callback err, stocks


    ], (err, stocks) ->
      callback err, stocks


  postStock: (data,callback) ->
    async.waterfall [
      (callback) ->
        if (ids = _.pluck(data, 'id')).length
          callback null, ids
        else
          callback true

      (ids,callback) ->
        table.storeStock.find {id: ids}, (err, result) ->
          return callback? err, result if err

          for ss in result
            aa = _.find(data, {id: ss.id})
            for field of ss
              ss[field] = aa[field] if aa[field]?
            ss.save()


          callback? err, result

    ], callback



get = (table, data, callback) ->
  table.find data, callback


edit = (table, data, callback) ->
    return callback? 'error' if _.isEmpty(data)
    unless data?.id
      table.create data, (err, result) ->
        return callback? err, result

    else if data?.id
      table.get data.id, (err, result) ->
        return callback? err, result if err

        for field of result
          result[field] = data[field] if data[field]?

        result.save (err) ->
          callback? err, result



