
async = require 'async'
_ = require 'lodash'
crypto = require("crypto")
jwt = require('jwt-simple')
settings = require('./../settings/server')()

FB = require('fb')
FB.options
  appSecret: settings.fbSecret



hash = (passwd, salt, callback) ->
  crypto.pbkdf2 passwd, salt, 70000, 512/32, (err, key) ->
    str = Buffer(key, 'binary').toString('hex')
#    console.log str
    callback? err, str




module.exports = (table) ->
  storePermissions = (tmpuser, callback) ->
    user = _.clone(tmpuser)
    table.storePermissions.find {user_id: user.id}, (err, tmpperms) ->
      perms = _.clone(tmpperms)
      if perms?
        table.store.find {id: _.pluck(perms, 'company_id')}, (err, stores) ->

          for perm in perms
            continue unless perm.company_id
            tmpstore = _.find(stores, {id:perm.company_id})
            perm.slug = tmpstore.slug
            perm.name = tmpstore.name

          user.store_permissions = perms
          return callback?(null, user)
      else
        return callback?(null, user)



  getUser: (data, callback) ->
    async.waterfall [
      (callback) ->
#        console.log Object.keys( table.user.allProperties )
        table.user.find {}, callback

    ], callback

  getProfile: (data, callback) ->
    async.waterfall [
      (callback) ->
        table.user.find( {id: data.id}).first callback
      (user, callback) ->
        table.userProfile.find( {user_id: data.id} ).first (err, profile) ->
          user?.profile = profile
          callback(err, user)

    ], callback


  login: (data, callback) ->
    async.waterfall [
      (callback) ->
        return callback(true) unless data?.email? and data.password?
        hash data.password, "qDg343dA4fqr", callback
#        hash data.password, "#{data.email}#{data.password}", callback
      (hash, callback) ->
        query = { or:[{email: data.email},{username: data.email}], password2: hash}
        table.user.find(query ).first callback
      (user, callback) ->
        storePermissions user, callback

      (user, callback) ->
        user.token = jwt.encode({id: user.id, password: user.password2, created: new Date()}, settings.jwtUser) if user?
        callback(null, user)

    ], callback

  tokenLogin: (data, callback) ->
    console.log data
    async.waterfall [
      (callback) ->
        return callback? true, {} unless data?.token?
        token = jwt.decode data.token, settings.jwtUser
        table.user.find({id:token.id, password2: token.password}).first callback
      (user, callback) ->
        storePermissions user, callback
    ], callback


  fbLogin: (data, callback) ->
    async.waterfall [
      (callback) ->
        FB.api 'me', {access_token: data.accessToken}, (res) ->
          callback null, res
      (fbData, callback) ->
        table.userProfile.find( {facebook_id: fbData.id}).first callback

      (profile, callback) ->
        query = { id: profile?.user_id}
        table.user.find(query ).first callback

      (user, callback) ->
        storePermissions user, callback

      (user, callback) ->
        user.token = jwt.encode({id: user.id, password: user.password2, created: new Date()}, settings.jwtUser) if user?
        callback(null, user)

    ], callback



