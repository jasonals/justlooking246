async = require 'async'
_ = require 'lodash'


module.exports = (table) ->

  getCategory: (data,callback) ->
    table.category.find data, callback

  postCategory: (data, callback) ->
    edit( table.category, data, callback)
#    callback? null, data





get = (table, data, callback) ->
  table.find data, callback

edit = (table, data, callback) ->
    return callback? 'error' if _.isEmpty(data)
    unless data?.id
      table.create data, (err, result) ->
        return callback? err, result

    else if data?.id
      table.get data.id, (err, result) ->
        return callback? err, result if err

        for field of result
          result[field] = data[field] if data[field]?

        result.save (err) ->
          callback? err, result



