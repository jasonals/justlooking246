async = require 'async'
_ = require 'lodash'


module.exports = (table) ->

  getItem: (data,callback) ->
    async.waterfall [
      (callback) ->
        query =
          active: true
        query.product_id = data.id if data?.id?
#        console.log query
        table.storeStock.find( query ).run callback

      (stocks, callback) ->
          storeID = _.chain(stocks).pluck('company_id').uniq().value()
          table.store.find {id: storeID}, (err, stores) ->
            for stock in stocks
              s = _.find( stores, {id: stock.company_id})
              stock.company_name = s?.name
              stock.company_slug = s?.slug

            callback err, stocks

      (stocks, callback) ->
        itemID = data?.id or _.chain(stocks).pluck('product_id').uniq().value()

#        table.item.find({id: id}).offset( (page - 1) * limit ).limit(limit).run (err, items) ->
        table.item.find({id: itemID}).run (err, items) ->
          for item in items
            item.store = _.filter( stocks, {product_id: item.id})
          callback err, items

    ], (err, items) ->
      callback err, items

  postItem: (data, callback) ->
    console.log data
    edit( table.item, data, callback)
#    callback? null, data


  getItemAction: (data, callback) ->
    async.waterfall [
      (callback) ->
#        console.log data
        table.userActionProduct.find {}, callback
    ], callback

#    get(table.product, data, callback)




get = (table, data, callback) ->
#  console.log data, "data"
  table.find data, callback

edit = (table, data, callback) ->
    return callback? 'error' if _.isEmpty(data)
    unless data?.id
      table.create data, (err, result) ->
        return callback? err, result

    else if data?.id
      table.get data.id, (err, result) ->
        return callback? err, result if err

        for field of result
          result[field] = data[field] if data[field]?

        result.save (err) ->
          callback? err, result



