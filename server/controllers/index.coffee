_ = require 'lodash'

controllers = {}

module.exports = (tables) ->

  _.extend controllers, require('./item')(tables)
  _.extend controllers, require('./store')(tables)
  _.extend controllers, require('./category')(tables)

  _.extend controllers, require('./user')(tables)



  return controllers

