
module.exports = (primus, controller) ->

  primus.on 'connection', (socket) ->

    socket.on 'get:item', controller.getItem
    socket.on 'post:item', controller.postItem

    socket.on 'get:store', controller.getStore
    socket.on 'post:store', controller.postStore

    socket.on 'get:stock', controller.getStock
    socket.on 'post:stock', controller.postStock

    socket.on 'get:category', controller.getCategory
    socket.on 'post:category', controller.postCategory


    socket.on 'get:storePermission', controller.getStorePermission


    socket.on 'get:user', controller.getUser

    socket.on 'get:userProfile', controller.getProfile

    socket.on 'get:itemAction', controller.getItemAction

    socket.on 'post:login', controller.login

    socket.on 'post:tokenLogin', controller.tokenLogin

    socket.on 'post:fblogin', controller.fbLogin
