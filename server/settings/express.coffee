


module.exports = (express, app) ->

  app.set('views', global.dirname + '/app')

  app.enable('jsonp callback')

  app.use express.bodyParser
    uploadDir: global.dirname + '/tempUpload'
    keepExtensions: true

  app.use express['static'] "#{global.dirname}/public"
#  app.use express['static'] "#{global.dirname}/dist"

  app.use '/bower_components', express['static'] "#{global.dirname}/bower_components"
  app.use '/dev', express['static'] "#{global.dirname}/dev"
  app.use '/.tmp', express['static'] "#{global.dirname}/.tmp"


  app.set('view engine', 'jade')
