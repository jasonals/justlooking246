
module.exports = (express, app, primus) ->

  require('./express')(express, app)
  require('./primus')(primus)

  server = require('./server')()




  return {
    server: server
  }
