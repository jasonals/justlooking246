
module.exports = (db) ->

  userItemlistItem: db.define "accounts_itemlist_product",
    "product_id": Number
    "itemlist_id": Number



  userProfile: db.define "accounts_userprofile",
    "gender": String
    "access_token": String
    "user_id": Number
    "date_of_birth": Date
    "image": String
    "blog_url": String
    "website_url": String
    "facebook_profile_url": String
    "facebook_name": String
    "facebook_id": Number
    "about_me": String

  
  userItemList: db.define "accounts_itemlist",
    "created": Date
    "updated": Date
    "user_id": Number
    "title": String

  

  userActionProduct: db.define "accounts_useractionproduct",
    "product_id": Number
    "created": Date
    "user_id": Number
    "action_id": Number
    "facebook_id": String

  

  user: db.define "auth_user",
    "password2": String
    "date_joined": Date
    "last_login": Date
    "is_superuser":  Boolean
    "is_active":  Boolean
    "is_staff":  Boolean
    "password": String
    "email": String
    "last_name": String
    "first_name": String
    "username": String

  
#
#   : db.define "accounts_useraction",
#    "created": Date
#    "object_id": Number
#    "content_type_id": Number
#    "user_id": Number
#    "action_id": Number
#    "facebook_id": String

  
