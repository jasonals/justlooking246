module.exports = (db) ->


  storePermissions: db.define "Justlooking_companypermissions",
    "delete":  Boolean
    "edit":  Boolean
    "view":  Boolean
    "user_id": Number
    "company_id": Number



  brandPermissions: db.define "Justlooking_brandpermissions",
    "delete":  Boolean
    "edit":  Boolean
    "view":  Boolean
    "user_id": Number
    "brand_id": Number


  storeStock: db.define "Justlooking_stock",
    "item_no": String
    "quantity": Number
#    "url": String
    "active":  Boolean
    "price_discount":  Number
    "price_retail":  Number
    "product_id": Number
    "company_id": Number



  storeLocation: db.define "Justlooking_storelocation",
    "lng":  Number
    "lat":  Number
    "phone_number": String
    "address": String
    "name": String
    "company_id": Number


  store: db.define "Justlooking_company",
    "orders":  Boolean
    "facebook_pageID": String
    "icon": String
    "phone_number": String
    "contact_email": String
    "facebook_page": String
    "brand_connect_id": Number
    "active":  Boolean
    "photo": String
    "website": String
    "slug": String
    "name": String




  stockLocation: db.define "Justlooking_stock_location",
    "storelocation_id": Number
    "stock_id": Number



  storeSignup: db.define "Justlooking_retailersignup",
    "other_info": String
    "contact_phone": String
    "contact_email": String
    "contact_name": String
    "company_name": String


