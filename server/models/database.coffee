

  "Justlooking_order": {
    "amount": Number
    "order_date": Date
    "item_id": Number
    "store_id": Number
    "user_id": Number
    
  },
  "django_site": {
    "name": String
    "domain": String
    
  },
  "django_facebook_facebookuser": {
    "name": String
    "facebook_id": Number
    "user_id": Number
    
  },
  "registration_registrationprofile": {
    "activation_key": String
    "user_id": Number
    
  },
  "django_session": {
    "expire_date": Date
    "session_data": String
    "session_key": String
  },
  "geometry_columns": {
    "type": String
    "srid": Number
    "coord_dimension": Number
    "f_geometry_column": String
    "f_table_name": String
    "f_table_schema": String
    "f_table_catalog": String
  },
  "session": {
    "expire_data":  Number
    "session_data": String
    "session_key": String
  },
  "django_flatpage": {
    "registration_required":  Boolean
    "template_name": String
    "enable_comments":  Boolean
    "content": String
    "title": String
    "url": String
    
  },
  "django_flatpage_sites": {
    "site_id": Number
    "flatpage_id": Number
    
  },
  "south_migrationhistory": {
    "applied": Date
    "migration": String
    "app_name": String
    
  },
  "spatial_ref_sys": {
    "proj4text": String
    "srtext": String
    "auth_srid": Number
    "auth_name": String
    "srid":  Number
  },
  "thumbnail_kvstore": {
    "value": String
    "key": String
  },
  "auth_group": {
    "name": String
    
  },
  "auth_group_permissions": {
    "permission_id": Number
    "group_id": Number
    
  },
  "accounts_action": {
    "name": String
    
  },
  "Justlooking_productattribute": {
    "description": String
    "name": String
    
  },
  "Justlooking_productdetail": {
    "description": String
    "attribute_id": Number
    "product_id": Number
    
  },
  "auth_message": {
    "message": String
    "user_id": Number
    
  },

  "auth_user_groups": {
    "group_id": Number
    "user_id": Number
    
  },
  "auth_user_user_permissions": {
    "permission_id": Number
    "user_id": Number
    
  },


  "auth_permission": {
    "codename": String
    "content_type_id": Number
    "name": String
    
  },
  "django_content_type": {
    "model": String
    "app_label": String
    "name": String
    
  },

  "django_admin_log": {
    "change_message": String
    "action_flag": "smallint",
    "object_repr": String
    "object_id": String
    "content_type_id": Number
    "user_id": Number
    "action_time": Date
    
  },
