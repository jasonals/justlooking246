orm = require 'orm'
_ = require 'lodash'

tables = {}

module.exports = (db) ->

  _.extend tables, require('./items')(db)
  _.extend tables, require('./store')(db)
  _.extend tables, require('./lookups')(db)
  _.extend tables, require('./user')(db)



  return tables

