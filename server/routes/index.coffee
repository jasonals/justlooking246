
module.exports = (app, controllers) ->

  require('./base')(app, controllers)
  require('./upload')(app, controllers)
