async = require 'async'
fs = require 'fs'
path = require 'path'


module.exports = (app, controller) ->

  app.post '/item-edit', (req, res) ->
    console.log req.body, 'ooo'
    async.waterfall [
      (callback) ->
        return callback('no name') unless req.body?.name
        return callback()
      (callback) ->
        data = req.body
#        if req.files?.image?.size

#          data.photo = path.basename(req.files.image.path)

        controller.postItem data, callback
    ], (err, item) ->
      if err or not req.files?.image?.size
        fs.unlink(req.files.image.path) if req.files?.image?.path
      if err
        return res.send JSON.stringify({success:false}),{'Content-Type':'text/plain'}, 404
      res.send JSON.stringify({item:item}), {'Content-Type': 'text/plain'}, 200

