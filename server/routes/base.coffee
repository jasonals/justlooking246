
index = (req, res) ->
  res.render 'browser/index'

privacyPolicy = about = termsOfUse = product = store  = index



module.exports = (app, controller) ->
  product = (req, res) ->
    controller.getItem {id: req.params?.id}, (err, item) ->
      res.render 'browser/base/product-nojs', {item: item?[0] or {}}

  store = (req, res) ->
    controller.getStore {slug: req.params?.slug}, (err, store) ->
      res.render 'browser/store/store-nojs', {store: store?[0] or {}}



  app.all '/', index


  app.all '/categories', index
  app.all '/map', index
  app.all '/pageviews', index
  app.all '/calendar', index

  app.all '/plans', index

  app.all '/contact', index

  app.all '/privacy-policy', privacyPolicy
  app.all '/about', about
  app.all '/terms-of-use', termsOfUse

  app.all '/search', index

  app.all '/product/:id*', product
  #app.get '/product/:id/:slug', product

  app.all '/stores', index
  app.all '/store/:slug', store
  app.all '/store/:slug/info', store


  app.all '/store/:slug/settings', index
  app.all '/store/:slug/settings/stock', index
  app.all '/store/:slug/settings/permissions', index
  app.all '/store/:slug/settings/products/edit', index
  app.all '/store/:slug/settings/products/bulkedit', index
  app.all '/store/:slug/settings/orders', index

  app.all '/brand*', index

  app.all '/resetpassword', index
  app.all '/login', index
  app.all '/register', index


  app.all '/user/:username', index
  app.all '/user/:username/settings', index
  app.all '/user/:username/orders', index
  app.all '/user/:username/likes', index
  app.all '/user/:username/list/:listId', index


  app.get '/admin', index
  app.get '/admin/user', index

#  app.all '/fbpagetab', fbpagetab
#  app.all '/fbpagetab/*', fbpagetab
#
#  app.get '/sitemap.xml', sitemap
#  app.get '/sitemap.xml.gz', sitemapGzip
