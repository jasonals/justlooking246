require 'coffee-script'
orm = require 'orm'
fs = require 'fs'
async = require 'async'

settings = require('./server/settings/server')()
db = require('orm').connect(settings.dbstr)
tables = require('./server/models')(db)

doneCount = 0
done = ->
  doneCount++
  if doneCount is 2
    process.exit()

#tables.item.find({photo: orm.eq(null) }).each( (item)-> item.photo = "#{item.id}.jpg" ).save( (err) -> console.log("done"); process.exit() )
#tables.item.find({photo: orm.eq(null) }).each( (item)-> item.photo = "#{item.id}.jpg" ).save( (err) -> console.log("done"); process.exit() )
tables.store.find().each( (store)-> store.photo = null ).save( (err) -> console.log("done"); process.exit() )

fs.readdir './public/media/product/original', (err, data) ->
  ids = (file.replace('.jpg','') for file in data when file.indexOf('.jpg') > -1)

  tables.item.find({id:ids})
  .each (item) ->
      item.photo = "#{item.id}.jpg"
  .save (err) ->
    console.log err if err
    console.log "done"
    done()

fs.readdir './public/media/store/original', (err, data) ->
  ids = (file.replace('.jpg','') for file in data when file.indexOf('.jpg') > -1)

  tables.store.find({id:ids})
  .each (store) ->
      store.photo = "#{store.id}.jpg"
  .save (err) ->
    console.log err if err
    console.log "done"
    done()
