global.dirname = __dirname

require 'coffee-script'

express = require 'express'
app = express()
server = require('http').createServer(app)

Primus = require 'primus'
primus = new Primus(server, {transformer: 'engine.io'})

settings = require('./server/settings')(express, app, primus)

db = require('orm').connect(settings.server.dbstr)
tables = require('./server/models')(db)

controllers = require('./server/controllers')(tables)

require('./server/sockets')(primus, controllers)
require('./server/routes')(app, controllers)

tables.store.find (err, cnt) -> console.log cnt.length
#tables.product.find {}, (err, products) ->
#  console.log products


server.listen(settings.server.port)

console.log "Server running on port #{settings.server.port}"




